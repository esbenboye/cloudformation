AWSTemplateFormatVersion: 2010-09-09

Description: 'Boiler plate fargate service'

Parameters:
  ProjectName:
    Description: "Input project name"
    Type: String
    Default: "boilerplate"
    AllowedPattern: "[0-9a-z-]+"
    ConstraintDescription: "All lowercase, 0-9 and dashes (-)"

  Environment:
    Description: "Input environment"
    Type: String
    Default: "test"

Mappings:
  BoilerplateCpuMappings:
    test:
      Cpu: 512
      Memory: 1024
    preprod:
      Cpu: 512
      Memory: 1024
    prod:
      Cpu: 1024
      Memory: 2048

Resources:
  BoilerplateSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      VpcId: !ImportValue AuctionVpcId
      GroupDescription: !Sub '${ProjectName}-SecurityGroup-${Environment}'
      SecurityGroupIngress:
      - IpProtocol: tcp
        FromPort: 80
        ToPort: 80
        CidrIp: '0.0.0.0/0'

  BoilerplateFargateService:
    Type: AWS::ECS::Service
    Properties:
      ServiceName: !Sub '${ProjectName}-${Environment}'
      Cluster: !ImportValue AuctionClusterId
      DesiredCount: 1
      LaunchType: FARGATE
      TaskDefinition: !Ref BoilerplateTaskDefinition
      NetworkConfiguration:
        AwsvpcConfiguration:
          AssignPublicIp: DISABLED
          SecurityGroups: 
            - !Ref BoilerplateSecurityGroup
          Subnets: 
            - !ImportValue PrivateSubnet00Id
            - !ImportValue PrivateSubnet01Id
      LoadBalancers:
        - TargetGroupArn: 
            Fn::ImportValue:
              !Sub '${ProjectName}-TargetGroupArn-${Environment}'
          ContainerName: !Sub '${ProjectName}-${Environment}'
          ContainerPort: 80 

  BoilerplateTaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      ContainerDefinitions:
        - LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Sub 'ecs/${ProjectName}-${Environment}'
              awslogs-region: !Sub ${AWS::Region}
              awslogs-stream-prefix: 'ecs'
              awslogs-create-group: true
          PortMappings:
            - HostPort: 80
              Protocol: tcp
              ContainerPort: 80
          Secrets:
            - Name: DB_USERNAME
              ValueFrom:
                Fn::ImportValue:
                  !Sub "${ProjectName}-DbUsernameSecret-${Environment}"

            - Name: DB_PASSWORD
              ValueFrom:
                Fn::ImportValue:
                  !Sub "${ProjectName}-DbPasswordSecret-${Environment}"

          Environment:
            - Name: DB_NAME
              Value:
                Fn::ImportValue:
                  !Sub "${ProjectName}-DbName-${Environment}"

            - Name: DB_READER_ENDPOINT
              Value: 
                Fn::ImportValue:
                  !Sub "${ProjectName}-ClusterReadEndpoint-${Environment}"

            - Name: DB_WRITER_ENDPOINT
              Value: 
                Fn::ImportValue:
                  !Sub "${ProjectName}-ClusterWriteEndpoint-${Environment}"

            - Name: AWS_REGION
              Value: !Sub ${AWS::Region}

            - Name: SQS_URL
              Value: 
                Fn::ImportValue:
                  !Sub '${ProjectName}-QueueUrl-${Environment}'

            - Name: SNS_TOPIC_ARN
              Value: 
                Fn::ImportValue:
                  !Sub 'EventBusArn-${Environment}'

            - Name: S3_BUCKET_NAME
              Value:
                Fn::ImportValue:
                  !Sub '${ProjectName}-BucketName-${Environment}'

          Image: !Sub '${AWS::AccountId}.dkr.ecr.${AWS::Region}.amazonaws.com/${ProjectName}/${Environment}:latest'
          Essential: true
          Name: !Sub '${ProjectName}-${Environment}'

      ExecutionRoleArn: 
        Fn::ImportValue:
          !Sub '${ProjectName}-TaskRoleArn-${Environment}'

      Family: !Sub '${ProjectName}-${Environment}'
      Cpu: !FindInMap [ BoilerplateCpuMappings, !Ref Environment, Cpu ]
      Memory: !FindInMap [ BoilerplateCpuMappings, !Ref Environment, Memory ]
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - "FARGATE"
      TaskRoleArn: 
        Fn::ImportValue:
          !Sub '${ProjectName}-TaskRoleArn-${Environment}'

